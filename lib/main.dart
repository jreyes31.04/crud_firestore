import 'package:crud_life/vistas/login.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  if (kIsWeb) {
    await Firebase.initializeApp(
      options: const FirebaseOptions(
        apiKey: "AIzaSyDuOkWMw9yVom6VaooxwvZAmYgXgyOMC4w",
        authDomain: "crud-life.firebaseapp.com",
        projectId: "crud-life",
        storageBucket: "crud-life.appspot.com",
        messagingSenderId: "142521887690",
        appId: "1:142521887690:web:56a8e514139f9ed8247d33",
      ),
    );
  } else {
    await Firebase.initializeApp();
  }
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        brightness: Brightness.dark,
      ),
      home: LoginView(),
    );
  }
}
