import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:crud_life/vistas/add_note.dart';
import 'package:crud_life/vistas/edit_note.dart';
import 'package:flutter/material.dart';

import '../utils/custom_colors.dart';
import '../utils/database.dart';

class FeedView extends StatefulWidget {
  const FeedView({super.key});

  @override
  State<FeedView> createState() => _FeedViewState();
}

class _FeedViewState extends State<FeedView> {
  Text customText(String txt) {
    return Text(
      txt,
      maxLines: 1,
      overflow: TextOverflow.ellipsis,
      style: const TextStyle(
        color: CustomColors.firebaseNavy,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(15),
          child: StreamBuilder<QuerySnapshot>(
            stream: Database.readItems(),
            builder: (context, snapshot) {
              if (snapshot.hasError) {
                return const Text(
                    'Hubo un error en la carga. Por favor intenta nuevamente en un rato');
              } else if (snapshot.hasData || snapshot.data != null) {
                return ListView.separated(
                  separatorBuilder: (context, index) =>
                      const SizedBox(height: 16.0),
                  itemCount: snapshot.data!.docs.length,
                  itemBuilder: (context, index) {
                    var noteInfo = snapshot.data!.docs[index].data()!
                        as Map<String, dynamic>;
                    String docID = snapshot.data!.docs[index].id;
                    String title = noteInfo['title'];
                    String note = noteInfo['note'];

                    return Ink(
                      decoration: BoxDecoration(
                        color: CustomColors.firebaseGrey,
                        borderRadius: BorderRadius.circular(8.0),
                      ),
                      child: ListTile(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8.0),
                        ),
                        onTap: () => Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => EditNote(
                              currentTitle: title,
                              currentNote: note,
                              noteId: docID,
                            ),
                          ),
                        ),
                        title: customText(title),
                        subtitle: customText(note),
                      ),
                    );
                  },
                );
              }

              return const Center(
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(
                    CustomColors.firebaseOrange,
                  ),
                ),
              );
            },
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => AddNote(),
            ),
          );
        },
        backgroundColor: CustomColors.firebaseOrange,
        child: const Icon(
          Icons.add,
          color: Colors.white,
          size: 32,
        ),
      ),
    );
  }
}
