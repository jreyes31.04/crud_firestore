import 'package:flutter/material.dart';

import '../utils/custom_colors.dart';
import '../utils/database.dart';
import '../utils/validator.dart';
import '../widgets/custom_form.dart';

class EditNote extends StatefulWidget {
  const EditNote({
    super.key,
    required this.currentTitle,
    required this.currentNote,
    required this.noteId,
  });

  final String currentTitle;
  final String currentNote;
  final String noteId;

  @override
  State<EditNote> createState() => _EditNoteState();
}

class _EditNoteState extends State<EditNote> {
  final _editItemFormKey = GlobalKey<FormState>();

  bool _isProcessing = false;

  late TextEditingController _titleController;
  late TextEditingController _noteController;

  final FocusNode _titleFocusNode = FocusNode();
  final FocusNode _noteFocusNode = FocusNode();

  @override
  void initState() {
    _titleController = TextEditingController(
      text: widget.currentTitle,
    );

    _noteController = TextEditingController(
      text: widget.currentNote,
    );

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.firebaseNavy,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: CustomColors.firebaseNavy,
        actions: [
          Visibility(
            visible: !_isProcessing,
            replacement: const Padding(
              padding: EdgeInsets.only(
                top: 10.0,
                bottom: 10.0,
                right: 16.0,
              ),
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(
                  Colors.redAccent,
                ),
                strokeWidth: 3,
              ),
            ),
            child: IconButton(
              icon: const Icon(
                Icons.delete,
                color: Colors.redAccent,
                size: 32,
              ),
              onPressed: () async {
                setState(() {
                  _isProcessing = true;
                });

                await Database.deleteItem(
                  docId: widget.noteId,
                );

                setState(() {
                  _isProcessing = false;
                });

                Navigator.of(context).pop();
              },
            ),
          ),
        ],
      ),
      body: Form(
        key: _editItemFormKey,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(
                left: 8.0,
                right: 8.0,
                bottom: 24.0,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(height: 24.0),
                  const Text(
                    'Título',
                    style: TextStyle(
                      color: CustomColors.firebaseGrey,
                      fontSize: 22.0,
                      letterSpacing: 1,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(height: 8.0),
                  CustomFormField(
                    controller: _titleController,
                    focusNode: _titleFocusNode,
                    inputAction: TextInputAction.next,
                    validator: (value) => Validator.validateField(
                      value: value,
                    ),
                    hint: 'Enter your note title',
                  ),
                  const SizedBox(height: 24.0),
                  const Text(
                    'Description',
                    style: TextStyle(
                      color: CustomColors.firebaseGrey,
                      fontSize: 22.0,
                      letterSpacing: 1,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(height: 8.0),
                  CustomFormField(
                    maxLines: 10,
                    controller: _noteController,
                    focusNode: _noteFocusNode,
                    inputAction: TextInputAction.done,
                    validator: (value) => Validator.validateField(
                      value: value,
                    ),
                    hint: 'Enter your note description',
                  ),
                ],
              ),
            ),
            Visibility(
              visible: !_isProcessing,
              replacement: const Padding(
                padding: EdgeInsets.all(16.0),
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(
                    CustomColors.firebaseOrange,
                  ),
                ),
              ),
              child: SizedBox(
                width: double.maxFinite,
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(
                      CustomColors.firebaseOrange,
                    ),
                    shape: MaterialStateProperty.all(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                  onPressed: () async {
                    _titleFocusNode.unfocus();
                    _noteFocusNode.unfocus();

                    if (_editItemFormKey.currentState!.validate()) {
                      setState(() {
                        _isProcessing = true;
                      });

                      await Database.updateItem(
                        docId: widget.noteId,
                        title: _titleController.text,
                        note: _noteController.text,
                      );

                      setState(() {
                        _isProcessing = false;
                      });

                      Navigator.of(context).pop();
                    }
                  },
                  child: const Padding(
                    padding: EdgeInsets.only(top: 16.0, bottom: 16.0),
                    child: Text(
                      'Actualizar nota',
                      style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                        color: CustomColors.firebaseGrey,
                        letterSpacing: 2,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
